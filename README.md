# DTN job interview assignment

## Application Details

This is a console application that reads lightning data as a stream from standard input to produce an alert.
The data used here is in JSON format. One lightning strike per line as a JSON object. On JSON file for the source of assets.
For each strike received, the application prints to the console the following message:

i.e.:

```log
lightning alert for 6720:Dante Street
```

If there are multiple lightning alerts was received in that location, the application will only log once per asset.

---

## How to Run the application

1. Download or Clone this repository to your machine

2. Open / double click on the .exe file found in this path: \bin\Debug\netcoreapp3.1\Lightning Alert.exe

3. A console application should pop up and the results will be printed out

4. Then the user needs to press any key to close the application.

---

### In addition, please answer the following questions:

- What is the [time complexity](https://en.wikipedia.org/wiki/Time_complexity) for determining if a strike has occurred for a particular asset? 
- If we put this code into production, but found it too slow, or it needed to scale to many more users or more frequent strikes, what are the first things you would think of to speed it up?