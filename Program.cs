﻿using System.IO;
using Newtonsoft.Json;
using LightningAlert.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using System.Reflection;
using log4net.Config;
using System.Diagnostics;

namespace LightningAlert
{
    class Program
    {
        // not sure if this is constant value
        // but based on the data, the lat long is somehow related on this
        private const int levelOfDetail = 12;

        // file names could be changed easilty
        private const string assetDataFileName = "assets.json";
        private const string lightningDataFileName = "lightning.json";

        // strings can be localized wihtout touching the main code
        private const string strLightningAlert = "({2}) lightning alert for {0}: {1}";
        private const string strExecutionComplete = "******* Execution was finished. Press any key to close the application.*******";
        private const string strTimeElapsed = "Time elapsed (ms): {0}";

        // we could directly change/assign the data file paths
        private static string currentPath = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
        private static string assetFilePath = currentPath + @"\" + assetDataFileName;
        private static string lightningFilePath = currentPath + @"\" + lightningDataFileName;

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            // Load the predefined data to be used
            // save clerical effort
            var assetData = LoadAssetData(assetFilePath);
            var lightningData = LoadLightningData(lightningFilePath);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //now that we already have all of the data we need, we can now proceed in processing them
            SendDistinctLightningAlerts(
                assetData: assetData,
                lightningData: lightningData);

            stopwatch.Stop();

            LogTimeElapsed(stopwatch);


            ExecutionCompleted();

            #region private methods 

            void SendDistinctLightningAlerts(List<Lightning> lightningData, List<Asset> assetData)
            {
                // we should store a list of notified assets to prevent alert duplicates
                var notifiedAssets = new List<string>();

                Asset asset;
                var assetDictionary = assetData.ToDictionary(a => a.quadKey);
                int count = 0;
                foreach (var lightning in lightningData)
                {
                    asset = GetAffectedAsset(lightning, assetDictionary, levelOfDetail);

                    // no affected assests, skip it
                    if (asset == null)
                        continue;

                    // do not send duplicate alerts
                    if (notifiedAssets.IndexOf(asset.quadKey) > -1)
                        continue;

                    // Alert our asset if matched!
                    AlertTheAsset(asset, count);
                    count++;

                    // take note of the assets we already notified
                    notifiedAssets.Add(asset.quadKey);
                }
            }

            List<Asset> LoadAssetData(string filePath)
            {
                var assetList = new List<Asset>();

                if (String.IsNullOrEmpty(filePath))
                    return assetList;

                using (StreamReader r = new StreamReader(filePath))
                {
                    try
                    {
                        string json = r.ReadToEnd();
                        assetList = JsonConvert.DeserializeObject<List<Asset>>(json);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }

                log.Info("Asset data successfully loaded.");
                return assetList;
            }

            List<Lightning> LoadLightningData(string filePath)
            {
                var lightningList = new List<Lightning>();

                if (String.IsNullOrEmpty(filePath))
                    return lightningList;

                using (StreamReader r = new StreamReader(filePath))
                {
                    //string json = r.ReadToEnd(); // put to array and Splice occurrence of '\r\n'

                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        try
                        {
                            // there is a possibility of bad data
                            var lightningEntry = JsonConvert.DeserializeObject<Lightning>(line);
                            lightningList.Add(lightningEntry);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message);
                            continue;
                        }
                    }
                }

                log.Info("Asset data successfully loaded.");
                return lightningList;
            }

            Asset GetAffectedAsset(Lightning lightning, Dictionary<string, Asset> assets, int? zoomLevel)
            {
                if (zoomLevel == null)
                    zoomLevel = 12;

                var key = lightning.GetQuadKey(zoomLevel.Value);

                if (!assets.ContainsKey(key))
                    return null;

                return assets[key];
            }

            void AlertTheAsset(Asset asset, int count)
            {
                Console.WriteLine();
                // we can send alert in other ways than just printing in the console
                Console.WriteLine(String.Format(strLightningAlert, asset.quadKey, asset.assetName, count));
                // we can add extra more lines too
            }

            void LogTimeElapsed(Stopwatch watch)
            {
                Console.WriteLine();
                Console.WriteLine(strTimeElapsed, stopwatch.Elapsed.TotalMilliseconds);
            }

            void ExecutionCompleted()
            {
                Console.WriteLine();
                // we can send an extraline to tell the user it's done
                Console.WriteLine(strExecutionComplete);
                // we can wait for the user before closing the console
                Console.ReadKey();
                // or we can do more stuff afterwards
                // without changing the inner logic
            }

            #endregion
        }
    }
}
