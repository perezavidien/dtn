﻿namespace LightningAlert.Models
{
    public class Asset
    {
        public Asset()
        { }

        public int id { get; set; } //no value provided
        public string assetName { get; set; } //Sunshine Wall
        public string quadKey { get; set; } //023112133002
        public string assetOwner { get; set; } //02115
    }
}
