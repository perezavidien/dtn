﻿using Microsoft.MapPoint;

namespace LightningAlert.Models
{
    public class Lightning : IGeoLocation
    {
        public Lightning(
            int flashType,
            string strikeTime,
            double latitude,
            double longitude,
            int peakAmps,
            string reserved,
            int icHeight,
            string receivedTime,
            int numberOfSensors,
            int multiplicity
            )
        {
            //add guards here
            this.flashType = flashType;
            this.strikeTime = strikeTime;
            this.latitude = latitude;
            this.longitude = longitude;
            this.peakAmps = peakAmps;
            this.reserved = reserved;
            this.icHeight = icHeight;
            this.receivedTime = receivedTime;
            this.numberOfSensors = numberOfSensors;
            this.multiplicity = multiplicity;
        }

        protected int flashType { private get; set; } //1
        protected string strikeTime { private get; set; } //1446760905179
        public double latitude { get; set; } //-17.0413122
        public double longitude { get; set; } //-94.9762348
        protected int peakAmps { private get; set; } //-4929
        protected string reserved { private get; set; } //000
        protected int icHeight { private get; set; } //15946
        protected string receivedTime { private get; set; } //1446760915185
        protected int numberOfSensors { private get; set; } //20
        protected int multiplicity { private get; set; } //20


        public string GetQuadKey(int zoomLevel)
        {
            TileSystem.LatLongToPixelXY(this.latitude, this.longitude, zoomLevel, out var pixelX, out var pixelY);
            TileSystem.PixelXYToTileXY(pixelX, pixelY, out var tileX, out var tileY);
            return TileSystem.TileXYToQuadKey(tileX, tileY, zoomLevel);
        }
    }
}
