﻿namespace LightningAlert
{
    public interface IGeoLocation
    {
        double latitude { get; set; }
        double longitude { get; set; }
        string GetQuadKey(int zoomLevel);
    }
}
